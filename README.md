# Gyazo Uploader

Web API to upload images with EXIF removed to [Gyazo](https://gyazo.com)

## Requirements

This tool depends on [honzasp/rust-turbojpeg: Rust bindings for TurboJPEG library](https://github.com/honzasp/rust-turbojpeg), so requires the following library.

- libturbojpeg

## Quick start

You can quickly and easily start the API by executing the following command.

```bash
docker build -t gyazo-uploader:latest .
docker run --rm -e TOKEN=<gyazo token> -p 3000:3000 gyazo-uploader:latest
```

For information on how to upload images, please refer to [Usage](#usage).

## Build

To build the project, run the following command in your terminal:

```bash
cargo build --release
```

## Usage

- Running the API Server  
   To start the API server, use the following command with your Gyazo token:

  ```bash
  TOKEN=<gyazo token> ./gyazo-uploader
  ```

- Uploading an Image  
   To upload an image to the server, use the following curl command:

  ```bash
  curl http://localhost:3000 -X POST -F image=@/path/to/image
  ```
