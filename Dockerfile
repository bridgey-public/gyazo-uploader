FROM rust:1.84.1-slim-bookworm AS builder
RUN apt update && apt install -y libturbojpeg0-dev cmake nasm

WORKDIR /app

ENV TURBOJPEG_STATIC=1

COPY Cargo.toml .
RUN mkdir -p src \
  && echo 'fn main() {}' > src/main.rs \
  && cargo build --bins --release

COPY src src
RUN CARGO_BUILD_INCREMENTAL=true cargo build --bins --release

FROM gcr.io/distroless/cc-debian12
LABEL maintainer="bridgey4461@protonmail.ch"

WORKDIR /gyazo-uploader
COPY --from=builder /app/target/release/gyazo-uploader . 
EXPOSE 3000
ENTRYPOINT [ "./gyazo-uploader" ] 
