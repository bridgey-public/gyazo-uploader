pub fn load_jpeg(data: &[u8]) -> Vec<u8> {
    // decompress and compress to remove exif
    let img: image::RgbImage = turbojpeg::decompress_image(data).expect("decompress error");
    let jpeg_data =
        turbojpeg::compress_image(&img, 95, turbojpeg::Subsamp::Sub2x2).expect("compress error");

    jpeg_data.as_ref().into()
}
