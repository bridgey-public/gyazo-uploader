use reqwest::multipart::{Form, Part};
use reqwest_middleware::ClientBuilder;
use reqwest_tracing::TracingMiddleware;
use serde::Deserialize;
use std::env;
use std::time::Duration;

#[derive(Deserialize)]
struct Gyazo {
    #[serde(rename = "type")]
    item_type: String,
    thumb_url: String,
    created_at: String,
    image_id: String,
    permalink_url: String,
    url: String,
}

pub async fn upload_image(image_buf: &[u8]) -> String {
    tracing::info!("Run upload_image");

    let token = match env::var("TOKEN") {
        Ok(val) => val,
        Err(_) => return "fail to load gyazo token from env".to_string(),
    };
    let url = "https://upload.gyazo.com/api/upload";

    let reqwest_client = reqwest::ClientBuilder::new()
        .timeout(Duration::from_secs(30))
        .build()
        .unwrap();
    let client = ClientBuilder::new(reqwest_client)
        .with(TracingMiddleware::default())
        .build();
    let part = Part::bytes(image_buf.to_owned()).file_name("image.png");
    let file = Form::new().part("imagedata", part);

    let result = client
        .post(url)
        .bearer_auth(token)
        .multipart(file)
        .send()
        .await;

    tracing::trace!(?result);

    match result {
        Ok(res) => {
            let gyazo_result = res.json::<Gyazo>().await;
            match gyazo_result {
                Ok(response) => {
                    tracing::info!("Success to upload image to gyazo");
                    return response.permalink_url;
                }
                Err(_) => {
                    tracing::warn!("Fail to upload image to gyazo");
                    return "Upload failure".to_string();
                }
            };
        }
        Err(_) => {
            tracing::warn!("Connection failure");
            return "Failure".to_string();
        }
    }
}
