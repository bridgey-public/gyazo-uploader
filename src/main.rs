use axum::{
    extract::{DefaultBodyLimit, Multipart},
    routing::post,
    Router,
};
use tower_http::trace::{self, TraceLayer};
use tracing::Level;

mod gyazo;
mod image_processing;

use gyazo::upload_image;
use image_processing::load_jpeg;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();

    let app = Router::new()
        .route("/gyazo", post(upload))
        .layer(
            TraceLayer::new_for_http()
                .make_span_with(trace::DefaultMakeSpan::new().level(Level::INFO))
                .on_response(trace::DefaultOnResponse::new().level(Level::INFO)),
        )
        .layer(DefaultBodyLimit::disable());

    let listener = tokio::net::TcpListener::bind("0.0.0.0:3000").await.unwrap();
    tracing::debug!("listening on {:?}", listener);
    axum::serve(listener, app).await.unwrap();
}

async fn upload(mut multipart: Multipart) -> String {
    tracing::trace!(?multipart);

    while let Ok(Some(mut field)) = multipart.next_field().await {
        let content_type = field.content_type().unwrap();

        let mut image_buf: Vec<u8> = Vec::new();
        if content_type == "image/jpeg" {
            let mut file_data = Vec::new();
            while let Some(chunk) = field.chunk().await.unwrap() {
                file_data.extend_from_slice(&chunk);
            }

            image_buf = load_jpeg(&file_data);
        } else if content_type == "image/png" {
            while let Some(chunk) = field.chunk().await.unwrap() {
                image_buf.extend_from_slice(&chunk);
            }
        } else {
            tracing::warn!("Unsupported format: {}", content_type);
            return "unsupported format.".to_string();
        }

        return upload_image(&image_buf).await;
    }
    tracing::warn!("Bad request");
    "Bad request".to_string()
}
